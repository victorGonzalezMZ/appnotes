package com.mx.victorgonzalez.appnotas.UI.Notes

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.mx.victorgonzalez.appnotas.Core.Resource
import com.mx.victorgonzalez.appnotas.Data.Local.AppDatabase
import com.mx.victorgonzalez.appnotas.Data.Local.LocalDataSource
import com.mx.victorgonzalez.appnotas.Data.Model.Note
import com.mx.victorgonzalez.appnotas.Data.Remote.ApiClient
import com.mx.victorgonzalez.appnotas.Data.Remote.NoteDataSource
import com.mx.victorgonzalez.appnotas.Presentation.NoteViewModel
import com.mx.victorgonzalez.appnotas.Presentation.NoteViewModelFactory
import com.mx.victorgonzalez.appnotas.R
import com.mx.victorgonzalez.appnotas.Repository.NoteRepositoryImp
import com.mx.victorgonzalez.appnotas.UI.Notes.Adapters.NoteAdapter
import com.mx.victorgonzalez.appnotas.databinding.FragmentNotesBinding

class NotesFragment : Fragment(R.layout.fragment_notes) {
    private lateinit var binding: FragmentNotesBinding
    private lateinit var adapter: NoteAdapter

    private val viewModel by viewModels<NoteViewModel> {
        NoteViewModelFactory(
            NoteRepositoryImp(
                LocalDataSource(AppDatabase.getDataBase(this.requireContext()).noteDao()),
                NoteDataSource(ApiClient.service)
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentNotesBinding.bind(view)

        binding.recyclerNotes.layoutManager = GridLayoutManager(requireContext(), 2)

        onLoading()

        binding.btnAddNote.setOnClickListener {
            val action = NotesFragmentDirections.actionNotesFragmentToNoteEditFragment2()
            findNavController().navigate(action)
        }

        binding.swipeRefresh.setOnRefreshListener {
            onLoading()
        }
    }

    private fun onLoading(){
        viewModel.fetchNotes().observe(viewLifecycleOwner, Observer { result ->
            when(result){
                is Resource.Loading -> {
                    //binding.progressbar.visibility = View.VISIBLE
                    binding.swipeRefresh.isRefreshing = true
                }
                is Resource.Success -> {
                    //binding.progressbar.visibility = View.GONE
                    adapter = NoteAdapter(result.data.data){ note ->
                        onNoteClick(note)
                    }
                    binding.recyclerNotes.adapter = adapter
                    binding.swipeRefresh.isRefreshing = false
                    Log.i("LiveData", "${result.data.toString()}")
                }
                is Resource.Failure -> {
                    //binding.progressbar.visibility = View.GONE
                    binding.swipeRefresh.isRefreshing = false
                    Log.e("LiveData", "${result.exception.toString()}")
                }
            }
        })
    }

    private fun onNoteClick(note: Note){
        val action = NotesFragmentDirections.actionNotesFragmentToNoteDetailFragment(
            note.id.toString(),
            note.title,
            note.content,
            note.image
        )

        findNavController().navigate(action)
    }
}