package com.mx.victorgonzalez.appnotas.UI.NoteDetails

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mx.victorgonzalez.appnotas.Core.Resource
import com.mx.victorgonzalez.appnotas.Data.Local.AppDatabase
import com.mx.victorgonzalez.appnotas.Data.Local.LocalDataSource
import com.mx.victorgonzalez.appnotas.Data.Model.Note
import com.mx.victorgonzalez.appnotas.Data.Remote.ApiClient
import com.mx.victorgonzalez.appnotas.Data.Remote.NoteDataSource
import com.mx.victorgonzalez.appnotas.Presentation.NoteViewModel
import com.mx.victorgonzalez.appnotas.Presentation.NoteViewModelFactory
import com.mx.victorgonzalez.appnotas.R
import com.mx.victorgonzalez.appnotas.Repository.NoteRepositoryImp
import com.mx.victorgonzalez.appnotas.databinding.FragmentNoteEditBinding

class NoteEditFragment : Fragment(R.layout.fragment_note_edit) {

    private lateinit var binding: FragmentNoteEditBinding

    private val viewModel by viewModels<NoteViewModel> {
        NoteViewModelFactory(NoteRepositoryImp(
            LocalDataSource(AppDatabase.getDataBase(this.requireContext()).noteDao()),
            NoteDataSource(ApiClient.service)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentNoteEditBinding.bind(view)

        binding.btnAddNote.setOnClickListener {
            var note = Note(0,
                binding.editTitle.text.toString(),
                binding.editContent.text.toString(),
                binding.editImageUrl.text.toString()
            )

            viewModel.saveNote(note).observe(viewLifecycleOwner, Observer { result ->
                when(result){
                    is Resource.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                        Log.d("LiveData", "Loading data...")
                    }
                    is Resource.Success -> {
                        binding.progressbar.visibility = View.GONE
                        findNavController().popBackStack()
                    }
                    is Resource.Failure -> {
                        binding.progressbar.visibility = View.GONE
                        Toast.makeText(requireContext(), "${result.exception.toString()}", Toast.LENGTH_LONG).show()
                        Log.d("LiveData", "${result.exception.toString()}")
                    }
                }
            })
        }
    }
}