package com.mx.victorgonzalez.appnotas.UI.Notes.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mx.victorgonzalez.appnotas.Data.Model.Note
import com.mx.victorgonzalez.appnotas.R
import com.mx.victorgonzalez.appnotas.databinding.ItemNoteBinding
import com.squareup.picasso.Picasso

class NoteAdapter(private val notes: List<Note>, private val listener:(Note) -> Unit) :
    RecyclerView.Adapter<NoteAdapter.viewHolder>(){

    class viewHolder(v: View) : RecyclerView.ViewHolder(v){
        val binding = ItemNoteBinding.bind(v)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.item_note, parent, false)
        return viewHolder(v)
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val note = notes[position]

        holder.itemView.setOnClickListener {
            listener(note)
        }

        Picasso.get().load(note.image).into(holder.binding.imgNota)
    }

    override fun getItemCount(): Int = notes.size

}