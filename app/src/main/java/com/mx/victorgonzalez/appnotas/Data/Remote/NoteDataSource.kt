package com.mx.victorgonzalez.appnotas.Data.Remote

import com.mx.victorgonzalez.appnotas.Data.Model.Note
import com.mx.victorgonzalez.appnotas.Data.Model.NoteList

class NoteDataSource(private val apiService: ApiService) {

    suspend fun getNotes(): NoteList = apiService.getNotes()

    suspend fun saveNote(note: Note?) : Note? = apiService.saveNote(note)

}