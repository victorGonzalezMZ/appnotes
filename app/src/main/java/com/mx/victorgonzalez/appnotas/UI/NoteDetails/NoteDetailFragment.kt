package com.mx.victorgonzalez.appnotas.UI.NoteDetails

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.mx.victorgonzalez.appnotas.R
import com.mx.victorgonzalez.appnotas.databinding.FragmentNoteDetailBinding
import com.squareup.picasso.Picasso

class NoteDetailFragment : Fragment(R.layout.fragment_note_detail) {

    private lateinit var binding: FragmentNoteDetailBinding
    private val args by navArgs<NoteDetailFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentNoteDetailBinding.bind(view)

        binding.textTitle.text = args.title
        binding.textContent.text = args.content

        Picasso.get().load(args.image).into(binding.imgNote)
    }
}