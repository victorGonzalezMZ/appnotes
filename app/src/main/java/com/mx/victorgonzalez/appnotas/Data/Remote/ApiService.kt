package com.mx.victorgonzalez.appnotas.Data.Remote

import com.mx.victorgonzalez.appnotas.Data.Model.Note
import com.mx.victorgonzalez.appnotas.Data.Model.NoteList
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {

    @GET("notes")
    suspend fun getNotes(): NoteList

    @POST("notes")
    suspend fun saveNote(@Body note: Note?) : Note?
}