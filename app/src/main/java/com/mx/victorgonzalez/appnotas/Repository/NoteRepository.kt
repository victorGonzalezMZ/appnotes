package com.mx.victorgonzalez.appnotas.Repository

import com.mx.victorgonzalez.appnotas.Data.Model.Note
import com.mx.victorgonzalez.appnotas.Data.Model.NoteList

interface NoteRepository {
    suspend fun getNotes(): NoteList
    suspend fun saveNote(note: Note?): Note?
}