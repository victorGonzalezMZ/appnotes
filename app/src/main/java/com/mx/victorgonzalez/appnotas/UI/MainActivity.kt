package com.mx.victorgonzalez.appnotas.UI

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mx.victorgonzalez.appnotas.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}