package com.mx.victorgonzalez.appnotas.Repository

import com.mx.victorgonzalez.appnotas.Data.Local.LocalDataSource
import com.mx.victorgonzalez.appnotas.Data.Model.Note
import com.mx.victorgonzalez.appnotas.Data.Model.NoteList
import com.mx.victorgonzalez.appnotas.Data.Model.toNoteEntity
import com.mx.victorgonzalez.appnotas.Data.Remote.NoteDataSource

class NoteRepositoryImp(
    private val localDataSource: LocalDataSource,
    private val dataSource: NoteDataSource) : NoteRepository {

    override suspend fun getNotes(): NoteList {
        dataSource.getNotes().data.forEach{ note ->
            localDataSource.saveNote(note.toNoteEntity())
        }

        return localDataSource.getNotes()
    }

    override suspend fun saveNote(note: Note?): Note? {
        return dataSource.saveNote(note)
    }

    //override suspend fun getNotes(): NoteList = dataSource.getNotes()
    //override suspend fun saveNote(note: Note?): Note? = dataSource.saveNote(note)
}