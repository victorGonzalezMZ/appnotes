package com.mx.victorgonzalez.appnotas.Presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.mx.victorgonzalez.appnotas.Core.Resource
import com.mx.victorgonzalez.appnotas.Data.Model.Note
import com.mx.victorgonzalez.appnotas.Repository.NoteRepository
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class NoteViewModel (private val repository: NoteRepository) : ViewModel() {

    fun fetchNotes() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.getNotes()))
        }
        catch(e: Exception) {
            emit(Resource.Failure(e))
        }
    }

    fun saveNote(note: Note?) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.saveNote(note)))
        }
        catch(e: Exception) {
            emit(Resource.Failure(e))
        }
    }

}

class NoteViewModelFactory(private val repository: NoteRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(NoteRepository::class.java).newInstance(repository)
    }
}