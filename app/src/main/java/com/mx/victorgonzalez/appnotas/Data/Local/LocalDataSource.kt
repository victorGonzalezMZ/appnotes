package com.mx.victorgonzalez.appnotas.Data.Local

import com.mx.victorgonzalez.appnotas.Data.Model.Note
import com.mx.victorgonzalez.appnotas.Data.Model.NoteEntity
import com.mx.victorgonzalez.appnotas.Data.Model.NoteList
import com.mx.victorgonzalez.appnotas.Data.Model.toNoteList

class LocalDataSource(private val noteDao: NoteDao) {
    suspend fun getNotes(): NoteList = noteDao.getNotes().toNoteList()

    suspend fun saveNote(note: NoteEntity) = noteDao.saveNote(note)
}