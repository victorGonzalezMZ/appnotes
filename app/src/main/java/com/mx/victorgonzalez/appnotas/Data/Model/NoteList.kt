package com.mx.victorgonzalez.appnotas.Data.Model

data class NoteList(
    val data: List<Note> = listOf()
)