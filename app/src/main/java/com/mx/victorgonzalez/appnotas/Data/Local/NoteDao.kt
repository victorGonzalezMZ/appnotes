package com.mx.victorgonzalez.appnotas.Data.Local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mx.victorgonzalez.appnotas.Data.Model.NoteEntity

@Dao
interface NoteDao {

    @Query("SELECT * FROM noteEntity")
    suspend fun getNotes(): List<NoteEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveNote(note: NoteEntity)
}