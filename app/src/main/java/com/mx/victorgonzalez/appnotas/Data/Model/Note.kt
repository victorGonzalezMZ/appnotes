package com.mx.victorgonzalez.appnotas.Data.Model

data class Note(
    val id      : Int    = 0,
    val title   : String = "",
    val content : String = "",
    val image   : String = ""
)